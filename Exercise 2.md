a.) Explain how inheritance and polymorphism manifest in the following cases. Pay special attention to the different catch branches.

```java 
abstract class Problem extends Exception {} 
class WeirdProblem extends Problem {}
class TrickyProblem extends Problem {}

//Soo, class Problem is a parent class to other Problem classes. It's abstract and doesn't have 
//any methods. So it's purpose probably is to be a marker for other similar classes that should 
//inherit it. All child classes also undirectly extend Exception.

class Experiment {
    void perform() throws WeirdProblem, TrickyProblem {
        if (new java.util.Random().nextBoolean())
            throw new WeirdProblem();
        else
            throw new TrickyProblem();
    }
}

//Class with a method which at random throws WeirdProblem or TrickyProblem

class Experiment2 {
    void perform() throws Problem {
        if (new java.util.Random().nextBoolean())
            throw new WeirdProblem();
        else
            throw new TrickyProblem();
    }
}

//Class that does the same, as Experiment class, but...both WeirdProblem and TrickyProblem are 
//inherited from parent class. So it's a manifestation of polymorphism.

void main() {
    var e1 = new Experiment();
    var e2 = new Experiment2();

    try {
        e1.perform();
        e2.perform();
    }
    catch (WeirdProblem w) {}
    catch (TrickyProblem w) {}
    catch (Problem w) {}
}

//Here we are making instances of Experiment and Experiment2 classes. In try block we call 
//Experiment class's method perform() through and instance e1. And then we make the same with 
//Experiment2's instance e2.
//The catch block catches and processes every possible scenario. Nothing special with first two 
//problems, they are held directly. But the third catch block wil process any Problem type 
//exception that wasn't processed in first two. This may happen if we'll decide to add a new 
//Problem that will be inherited from Problem class.

```

 
b.) Compare the following reuse methods (Printer1 and Printer2). What is each one about? How do they differ in their operating principles? How do the different methods support, for example, changing the decoration style?
 
The previous Printer functionality needs to be extended in the Printer3 class so that instead of the previous decoration (decorate), the decoration adds the previously defined decoration to the beginning and end of the string, along with the characters --. What kind of implementation would this be? How would your solution differ if the number of -- characters used for decoration was not 2 per side but was read from a method described in the Decorator interface? What if it was in the Printer interface?

```java 
interface Decorator {
    String decorate(String input);
}

interface Printer {
    void print(String s);
    void run();
}

//two interfaces with methods that needs to be overriden.

class Fancy implements Decorator {
    @Override
    public String decorate(String input) {
        return "== " + input + " ==";
    }
}

//class Fancy with a default constructor. Method decorate() is overriden, added functionality

class Printer1 extends Fancy implements Printer {
    @Override
    public void print(String s) {
        System.out.println(s);
    }

    @Override
    public void run() {
        print(decorate("main"));
    }
}

//class Printer1 extends from Fancy (indirectly implements Decorator) and implements Printer interface. Both methods are overriden, as they should be. Method run() in it's routine calls parent's method decorate().

class Printer2 implements Decorator, Printer {
    private final Decorator decorator =
            generateDecorator();

    //here we are using composition that allows our code to be more flexible

    Decorator generateDecorator() {
        return new Fancy();
    }

    //class implements same interfaces as previous class, so we are able to make an instance of a class Fancy. 

    @Override
    public String decorate(String input) {
        return decorator.decorate(input);
    }

    //So we are adressing calls to decorator instance to Fancy class's method decorate.

    @Override
    public void print(String s) {
        System.out.println(s);
    }

    @Override
    public void run() {
        print(decorate("main"));
    }

    //two same Overrides that we saw in previous class. but run method uses decorate mrthod of Fancy class.
}
```

The following code demonstrates test runs with the aforementioned classes Printer1 and Printer2, as well as the Printer3 class, which should be implemented:

//I think class Printer3 may look like this

```java
class Printer3 implements Decorator, Printer {
    private final Decorator decorator = generateDecorator();

    Decorator generateDecorator() {
        return new Printer2();
    }

    @Override
    public String decorate(String input) {
        String decoratedInput = decorator.decorate(input);
        return "--" + decoratedInput + "--";
    }

    @Override
    public void print(String s) {
        System.out.println(s);
    }

    @Override
    public void run() {
        print(decorate("main"));
    }
}
```

//I think the solution will be quite same, if we need to read the number of characters that used for decoration, from interfaces Printer or Decorator. We need to add a method int getCharNumb() to the interface. In our class we Override this method. We can make it just return the quantity that we need (2 in our example). In method decorate() we are taking an int from getCharNumb() implement it to character that we want to decorate and return decorated string. 

```java
void main() {
    new Printer1().run();
    new Printer2().run();
    new Printer3().run();
}
Expected output:
== main ==
== main ==
--== main ==--
```